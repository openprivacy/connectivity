module git.openprivacy.ca/openprivacy/connectivity

go 1.23.1

require (
	filippo.io/edwards25519 v1.0.0
	git.openprivacy.ca/openprivacy/bine v0.0.5
	git.openprivacy.ca/openprivacy/log v1.0.3
	golang.org/x/crypto v0.0.0-20220826181053-bd7e27e6170d
)

require (
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
