package connectivity

import (
	"net"
)

// ErrorACN - a status-callback safe errored ACN. Use this when ACN construction goes wrong
// and you need a safe substitute that can later be replaced with a working ACN without impacting calling clients.
type ErrorACN struct {
	acnError             error
	statusCallbackCache  func(int, string)
	versionCallbackCache func(string)
}

func NewErrorACN(err error) ErrorACN {
	return ErrorACN{
		acnError:             err,
		statusCallbackCache:  func(int, string) {},
		versionCallbackCache: func(string) {},
	}
}

func (e *ErrorACN) GetStatusCallback() func(int, string) {
	return e.statusCallbackCache
}

func (e *ErrorACN) GetVersionCallback() func(string) {
	return e.versionCallbackCache
}

func (e *ErrorACN) GetInfo(addr string) (map[string]string, error) {
	return nil, e.acnError
}

func (e *ErrorACN) GetBootstrapStatus() (int, string) {
	return -1, e.acnError.Error()
}

func (e *ErrorACN) WaitTillBootstrapped() error {
	return e.acnError
}

func (e *ErrorACN) SetStatusCallback(callback func(int, string)) {
	e.statusCallbackCache = callback
}

func (e *ErrorACN) SetVersionCallback(callback func(string)) {
	e.versionCallbackCache = callback
}

func (e *ErrorACN) Restart() {
}

func (e *ErrorACN) Open(hostname string) (net.Conn, string, error) {
	return nil, "", e.acnError
}

func (e *ErrorACN) Listen(identity PrivateKey, port int) (ListenService, error) {
	return nil, e.acnError
}

func (e *ErrorACN) GetPID() (int, error) {
	return -1, e.acnError
}

func (e *ErrorACN) GetVersion() string {
	return e.acnError.Error()
}

func (e *ErrorACN) Close() {
	// nothing to do...
}
