//go:build !windows
// +build !windows

package tor

import (
	"syscall"
)

var sysProcAttr = &syscall.SysProcAttr{}
