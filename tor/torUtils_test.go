package tor

import (
	"encoding/hex"
	"filippo.io/edwards25519"
	"git.openprivacy.ca/openprivacy/bine/torutil"
	"os"
	"testing"
)

func TestGenerateHashedPassword(t *testing.T) {
	// 16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257
	hp := generateHashedPassword([8]byte{0xC1, 0x53, 0x05, 0xF9, 0x77, 0x89, 0x41, 0x4B}, "examplehashedpassword")
	if hp != "16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257" {
		t.Fatalf("hashed passwords do not match. Expected %s, got %s", "16:C15305F97789414B601259E3EC5E76B8E55FC56A9F562B713F3D2BA257", hp)
	}
}

func TestIsValidHostname(t *testing.T) {
	openprivonion := "openpravyvc6spbd4flzn4g2iqu4sxzsizbtb5aqec25t76dnoo5w7yd"
	t.Logf("testing: %v", openprivonion)
	if IsValidHostname(openprivonion) == false {
		t.Fatalf("open privacy onion should validate as a valid hostname")
	}

	sarahonion := "icyt7rvdsdci42h6si2ibtwucdmjrlcb2ezkecuagtquiiflbkxf2cqd"
	t.Logf("testing: %v", sarahonion)
	if IsValidHostname(sarahonion) == false {
		t.Fatalf("sarah onion should validate as a valid hostname")
	}

	// First we will construct a torsion point from our Valid Onion
	pubKey, _ := torutil.PublicKeyFromV3OnionServiceID(openprivonion)
	pubKeyPoint, _ := new(edwards25519.Point).SetBytes(pubKey)
	torsionPubKeyBytes, _ := hex.DecodeString("26e8958fc2b227b045c3f489f2ef98f0d5dfac05d3c63339b13802886d53fc05")
	torsionHostname, _ := torutil.PublicKeyFromV3OnionServiceID(GetTorV3Hostname(torsionPubKeyBytes))
	torsionPoint, _ := new(edwards25519.Point).SetBytes(torsionHostname)
	malformedKey := new(edwards25519.Point).Add(pubKeyPoint, torsionPoint)

	t.Logf("testing: %v", GetTorV3Hostname(malformedKey.Bytes()))
	if IsValidHostname(GetTorV3Hostname(malformedKey.Bytes())) == true {
		t.Fatalf("torsion onion should not validate as a valid hostname")
	}

	// Testing a few torsion points taken from https://lists.torproject.org/pipermail/tor-dev/2017-April/012226.html
	torsionPubKey, _ := hex.DecodeString("0000000000000000000000000000000000000000000000000000000000000000")
	t.Logf("testing: %v", GetTorV3Hostname(torsionPubKey))
	if IsValidHostname(GetTorV3Hostname(torsionPubKey)) == true {
		t.Fatalf("torsion onion should not validate as a valid hostname")
	}

	torsionPubKey, _ = hex.DecodeString("26e8958fc2b227b045c3f489f2ef98f0d5dfac05d3c63339b13802886d53fc05")
	t.Logf("testing: %v", GetTorV3Hostname(torsionPubKey))
	if IsValidHostname(GetTorV3Hostname(torsionPubKey)) == true {
		t.Fatalf("torsion onion should not validate as a valid hostname")
	}

	torsionPubKey, _ = hex.DecodeString("c9fff3af0471c28e33e98c2043e44f779d0427b1e37c521a6bddc011ed1869af")
	t.Logf("testing: %v", GetTorV3Hostname(torsionPubKey))
	if IsValidHostname(GetTorV3Hostname(torsionPubKey)) == true {
		t.Fatalf("torsion onion should not validate as a valid hostname")
	}

	torsionPubKey, _ = hex.DecodeString("f43e3a046db8749164c6e69b193f1e942c7452e7d888736f40b98093d814d5e7")
	t.Logf("testing: %v", GetTorV3Hostname(torsionPubKey))
	if IsValidHostname(GetTorV3Hostname(torsionPubKey)) == true {
		t.Fatalf("torsion onion should not should validate as a valid hostname")
	}

	torsionPubKey, _ = hex.DecodeString("300ef2e64e588e1df55b48e4da0416ffb64cc85d5b00af6463d5cc6c2b1c185e")
	t.Logf("testing: %v", GetTorV3Hostname(torsionPubKey))
	if IsValidHostname(GetTorV3Hostname(torsionPubKey)) == true {
		t.Fatalf("torsion onion should not validate as a valid hostname")
	}

	// this should pass
	// (also from https://lists.torproject.org/pipermail/tor-dev/2017-April/012230.html)
	validPubKey, _ := hex.DecodeString("4ba2e44760dff4c559ef3c38768c1c14a8a54740c782c8d70803e9d6e3ad8794")
	t.Logf("testing: %v", GetTorV3Hostname(validPubKey))
	if IsValidHostname(GetTorV3Hostname(validPubKey)) == false {
		t.Fatalf("valid onion should validate as a valid hostname")
	}

	// Finally test a completely invalid key...
	badPubKey, _ := hex.DecodeString("e19c65de75c68cf3b7643ea732ba9eb1a3d20d6d57ba223c2ece1df66feb5af0")
	t.Logf("testing: %v", GetTorV3Hostname(badPubKey))
	if IsValidHostname(GetTorV3Hostname(badPubKey)) == true {
		t.Fatalf("invalid ed25519 point should not validate as a valid hostname")
	}
}

func TestGenerateTorrc(t *testing.T) {
	path := "./torrc.test"
	password := "examplehashedpassword"
	err := NewTorrc().WithHashedPassword(password).Build(path)
	if err != nil {
		t.Errorf("Torrc file could not be written")
	}
	os.Remove(path)
}

func TestPreviewTorrc(t *testing.T) {
	expected := "SocksPort 9050 OnionTrafficOnly\nControlPort 9061"
	torrc := NewTorrc().WithCustom([]string{"SocksPort 9050"}).WithControlPort(9061).WithOnionTrafficOnly().Preview()
	if torrc != expected {
		t.Fatalf("unexpected torrc generated: [%v] [%v]", expected, torrc)
	}
}
