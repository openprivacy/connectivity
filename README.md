# connectivity

A library providing an ACN (Anonymous Communication Network
) networking abstraction

## Supported ACNs

* Tor v3 Onion Services

## Environment Variables

- `TOR_LD_LIBRARY_PATH` - override the library path given to the Tor process as different from the one given to the parent process.
- `CWTCH_RESTRICT_PORTS` - forces connectivity to bind to a subset of ports `15000-15378`
- `CWTCH_BIND_EXTERNAL_WHONIX` - forces connectivity to bind to external interfaces (only supported/recommended on certain Whonix-based setups. Please open an issue if you think this should be expanded.)

## Requirements for ACN Support

* Reference an EndPoint via a string / hostname
* Maintain an endpoint via a PublicKey (the underlying crypto is the
 responsibility of the implementation)
 
## Using

Each ACN implementation provides a specific start function that takes in the
required parameters to e.g. find a specific binary on the system, attempt to
 talk to a specific system service or launch an in-memory networking manager:

        acn, err := NewTorACN(".", "", 9051, HashedPasswordAuthenticator{"examplehasedpassword"})
        if err != nil {
            t.Error(err)
            return
        }

At this point the ACN is responsible for setting up the networking interface,
the result of which can be checked via the Status callback:
        
    
        acn.SetStatusCallback(getStatusCallback(progChan))
    
        progress := 0
        for progress < 100 {
            progress = <-progChan
        }

Once initialized the ACN can be used to open new connections:

        conn,err := acn.Open(hostname);

Or host a service on the ACN:

        ls,err := acn.Listen(identity, port) ;

We also provide closing and restart functionality for managing the networking
service:

        acn.Restart()
and
        
        acn.Close()
