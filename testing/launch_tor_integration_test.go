package testing

import (
	"git.openprivacy.ca/openprivacy/connectivity/tor"
	"git.openprivacy.ca/openprivacy/log"
	"math/rand"
	"os"
	path "path/filepath"
	"runtime"
	"runtime/pprof"
	"testing"
	"time"
)

func TestLaunchTor(t *testing.T) {
	goRoutineStart := runtime.NumGoroutine()
	log.SetLevel(log.LevelDebug)

	controlPort := rand.Intn(1000) + 9052
	password := "examplehashedpassword"

	// Create the tor data directory if it doesn't already exist..
	os.MkdirAll("../tmp/data/tor", 0700)
	err := tor.NewTorrc().WithControlPort(controlPort).WithHashedPassword(password).Build("../tmp/data/tor/torrc")

	if err != nil {
		t.Fatalf("failed to create torrc file: %v", err)
	}

	dataDir := ""
	if dataDir, err = os.MkdirTemp(path.Join("..", "testing"), "data-dir-"); err != nil {
		t.Fatalf("could not create data dir")
	}

	// Get the current working director, clean the paths to remove relative references
	wd, _ := os.Getwd()
	t.Logf("Launching bundled tor at %v", path.Clean(wd+"/../tmp/tor"))
	acn, err := tor.NewTorACNWithAuth(path.Clean(wd+"/../tmp/data"), path.Clean(wd+"/../tmp/tor"), dataDir, controlPort, tor.HashedPasswordAuthenticator{Password: password})
	if err != nil {
		t.Fatalf("tor failed to start: %v", err)
	} else {
		err := acn.WaitTillBootstrapped()
		if err != nil {
			t.Fatalf("error bootstrapping tor %v", err)
		}

		if pid, err := acn.GetPID(); err == nil {
			t.Logf("tor pid: %v", pid)
		} else {
			t.Fatalf("error fetching pid: %v", err)
		}
		t.Log("we have bootstrapped!")
		acn.Close()
	}

	time.Sleep(time.Second * 5)

	goRoutineEnd := runtime.NumGoroutine()

	if goRoutineEnd != goRoutineStart {
		pprof.Lookup("goroutine").WriteTo(os.Stdout, 1)

		t.Fatalf("goroutine leak in ACN: %v %v", goRoutineStart, goRoutineEnd)
	}

}
